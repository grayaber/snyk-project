var x_redha_pnt_devops = x_redha_pnt_devops || {};

x_redha_pnt_devops.onLoadRedirectHelpRed = function() {
	var WindowlocationPathName =  window.location.pathname,
		LocationSearch = location.search.substring(1), newUrl;
	
	if ( LocationSearch.indexOf('id=ticket') !== -1 ) {
		newUrl = LocationSearch.replace(/(id=).*?(&)/,'$1' + 'rh_ticket' + '$2');
		window.location.replace('/help?' + newUrl + '&pnt_redirection');

	} else if ( LocationSearch.indexOf('id=sc_home') !== -1 ) {
		window.location.replace('/help?id=rh_sc_home&sc_catalog=9ff7b3f513f96600daa77b304244b018&pnt_redirection');

	} else if ( 
		LocationSearch.indexOf('id=pnt_order_guide') !== -1 ||
		LocationSearch.indexOf('id=sc_cat_item&sys_id=139c5fe613766300dce03ff18144b09d') !== -1
	) {  
		window.location.replace('/help?pnt_redirection');
	
	} else if ( LocationSearch.indexOf('id=pnt_index') !== -1 ) {
		window.location.replace('/help?pnt_redirection');
		
	} else {
		var cleanLocationSearch = window.location.search;
			cleanLocationSearch = cleanLocationSearch.replace('?','');
		window.location.replace('/help?'+cleanLocationSearch+'&pnt_redirection');
	}

};

$(document).ready(x_redha_pnt_devops.onLoadRedirectHelpRed);
